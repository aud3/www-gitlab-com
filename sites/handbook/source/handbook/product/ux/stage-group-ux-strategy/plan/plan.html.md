---
layout: handbook-page-toc
title: Plan UX Team
description: "The Plan UX team supports the Plan section to provide customers a great experience with organizing, planning and tracking work across their GitLab projects."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

## How We Work

## UX Research



